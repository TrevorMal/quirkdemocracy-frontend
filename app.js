/**
 * Created by Trevor Malovhele on 2015/05/28.
 */
'use strict';
var democracy = angular.module('democracy', [
    'ngRoute',
    'ngCookies',
    'demoController'
]);

democracy.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        //.when('/', {
        //    templateUrl: 'partials/login.html'
        //})
        .when('/', {
            controller: 'LoginController',
            templateUrl: 'partials/login.html',
            controllerAs: 'qd'

        })
        .when('/dashboard', {
            templateUrl: 'partials/dashboard.html',
            controller: 'DashboardController',
            controllerAs: 'qd'
        })
        .when('/current-results', {
            templateUrl: 'partials/current-result.html',
            controller: 'ViewResultsController',
            controllerAs: 'qd'
        })
        .when('/existing-polls', {
            templateUrl: 'partials/existing-polls.html',
            controller: 'ExistingPollsController',
            controllerAs: 'qd'
        })
        .when('/ideas-wall', {
            templateUrl: 'partials/ideas-wall.html',
            controller: 'IdeasWallController',
            controllerAs: 'qd'
        })
        .when('/create-new-poll', {
            templateUrl: 'partials/create-polls.html',
            controller: 'CreatePollsController',
            controllerAs: 'qd'
        })
        .when('/feed-back', {
            templateUrl: 'partials/feedback.html',
            controller: 'FeedbackController',
            controllerAs: 'qd'
        })
        .otherwise({
            templateUrl: 'partials/error.html'
        });
    // $locationProvider.html5Mode(true);
}]);
//democracy.run(['$rootScope','$location', function ($rootScope,$location) {
//    $rootScope.title = "Welcome to Quirk Democrazy";
//    $rootScope.loggedInUser = '';
//    if(!$rootScope.loggedInUser){
//        $location.path('/');
//    }
//    console.log($rootScope);
//}]);
