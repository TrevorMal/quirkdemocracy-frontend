/**
 * Created by Trevor Malovhele on 2015/06/03.
 */
//Main dashboard controller
democracy.controller('DashboardController', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
    /*Set the defaults*/
    $scope.results = [];
    $scope.polls = [];
    $scope.message = 'Hello Welcome...';
    $scope.ExistingMsg = 'Start creating new polls right now!!';
    $scope.CreateMsg = 'Start creating new polls right now!!';
    $rootScope.title = 'Quirk Democracy | Dashboard';
    $scope.results = voteResults($scope,$http);
    $scope.polls = polls($scope,$http);
    console.log($scope.polls);
    //Socket.on('content:changed',function(data){
    //    $scope.data = data;
    //})
    //$scope.submitContent = function(){
    //    Socket.emit('content:changed',$scope.data);
    //}

}]);

function voteResults($scope,$http) {
    $http.get('http://localhost:3000/api/votingResults').success(function (data) {
       $scope.results = data;

    })
    return $scope;
}
function polls($scope,$http) {
    $http.get('http://localhost:3000/api/polls').success(function (data) {
        $scope.polls = data;

    })
    return $scope;
}

//democracy.factory('Socket',function($rootScope){
//    var socket = io.connect('http://localhost:3000');
//
//    return {
//        on : function(eventName,fn){
//            socket.on(eventName,function(data){
//               $rootScope.$apply(function(){
//                   fn(data);
//               }) ;
//            });
//        },
//        emit : socket.emit
//    }
//})