/**
 * Created by Trevor Malovhele on 2015/05/28.
 */

var demoController = angular.module('demoController', []);

democracy.controller('LoginController', ['$scope', '$location', '$rootScope', 'AuthService', function ($scope, $location, $rootScope, AuthService) {
    $rootScope.title = 'Quirk Democracy | Login';
    $scope.buttonText = 'Log In';
    $scope.status_message = 'Something went wrong somewhere, please try again';
    $rootScope.user = {};
    $rootScope.loggedInUser = '';
    $scope.authenticate = function () {

        $scope.buttonText = 'Logging In......';
        AuthService.login($scope.credentials.userName, $scope.credentials.userPassword).then(function (data) {
            $scope.invalidLogin = false;
            $location.path('/dashboard');
            if (data.status_message !== 'Authentication Failed') {
                $scope.invalidLogin = false;
                $scope.status_message = data.status_message;
                $rootScope.user = data;
                $rootScope.loggedInUser = data.user.user;
                $location.path('/dashboard');
            } else {
                $scope.invalidLogin = true;
                $location.path('/');
                $scope.status_message = data.status_message + ' Please try again.';
            }

        }, function (err) {
            $location.path('/');
            $scope.invalidLogin = true;

        }).finally(function () {
            $scope.buttonText = 'Log In';
        });

    }
}]);

democracy.controller('ViewResultsController', ['$scope', '$rootScope','$http', function ($scope,$rootScope,$http) {
    $rootScope.title = 'Quirk Democracy | View Results';
    $scope.results = voteResults($scope, $http);
    console.log($scope.results);

}]);

democracy.controller('ExistingPollsController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $rootScope.title = 'Quirk Democracy | Existing Polls';
    $scope.message = 'Welcome to the Existing Polls!!'
}]);

democracy.controller('CreatePollsController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $rootScope.title = 'Quirk Democracy | Create Poll';
    $scope.message = 'Start creating new polls right now!!'
}]);

democracy.controller('FeedbackController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $rootScope.title = 'Quirk Democracy | Feedback';
    $scope.message = 'We would like to know what you think :)'
}]);

democracy.controller('IdeasWallController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $rootScope.title = 'Quirk Democracy | Ideas Wall';
    $scope.message = 'We would like to know what you think :)'
}]);

democracy.controller('LogoutController', ['$scope', function ($rootScope) {
    $rootScope.loggedInUser = '';
    console.log('In dashboard controller');
}]);

function getUsers($scope, $http) {
    console.log('test' + $scope.userName);
    $http.get('http://localhost:3000/api/users/').success(function (data) {
        $scope.users = data;
        return $scope;
    })
}