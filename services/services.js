/**
 * Created by Trevor Malovhele on 2015/06/02.
 */
/*Service definitions*/
democracy.factory('AuthService', ['$http', function ($http) {
    var auth = {};

    auth.login = function (userName, userPassword) {
        return $http.post('http://localhost:3000/api/authenticate/', {
            username: userName,
            pass: userPassword
        }).then(function (response) {
            auth.user = response.data;
            //$cookiesStore.put('loggedInUser', auth.user);
            console.log(auth.user);
            return auth.user;
        });
    }
    auth.logout = function (userName) {
        auth.user = undefined;
        //$cookiesStore.remove('loggedInUser');
        return auth.user;
    }
    auth.setCurrentUser = function () {
        var authData = username;
        $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
    }
    auth.clearCurrentUser = function () {
        $http.defaults.headers.common['Authorization'] = 'Basic ';
    }

    return auth;
}]);